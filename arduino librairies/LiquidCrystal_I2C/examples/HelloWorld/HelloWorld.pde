//
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
int photocellPin = A3; // the cell and 10K pulldown are connected to a0
int photocellReading; // the analog reading from the analog resistor divider
void setup()
{
  lcd.init();     
  
  // Print a message to the LCD.
  lcd.backlight();
  lcd.clear();
  pinMode(A1, INPUT);

}

void loop()
{
	  photocellReading = analogRead(photocellPin);
	int volt = analogRead(A0);
	float folol = (volt/1023.0)*5.0;
	lcd.setCursor(0,0);
	lcd.print("Voltage: ");
	lcd.print(folol);

float b = map(volt, 0, 1023, 0, 256);
	analogWrite(11, b);

lcd.setCursor(0,1);
lcd.print("pwm:");
lcd.print(b);
lcd.setCursor(0,2);
lcd.print("phot=");
  lcd.print(photocellReading); // the raw analog reading
  // We'll have a few threshholds, qualitatively determined
  lcd.setCursor(0,3);
  if (photocellReading < 10) {
    lcd.println("Noir");
  } else if (photocellReading < 200) {
    lcd.println("Sombre");
  } else if (photocellReading < 500) {
    lcd.println("Lumiere");
  } else if (photocellReading < 800) {
    lcd.println("Lumix");
  } else {
    lcd.println("tlx");
  }
delay(500);
}
/* Photocell simple testing sketch.
Connect one end of the photocell to 5V, the other end to Analog 0.
 Then connect one end of a 10K resistor from Analog 0 to ground
For more information see http://learn.adafruit.com/photocells */

