# STAIRDUINO README #



### What is this Stairduino for? ###

* Stair steps lighting system , animated 
* Version v1.0
* 
* http://upverter.com/Alnoa/4c14758326a713d1/Stairduino/

### How do I get set up? ###

    Variables:
    #define capteurbot 5 : input pin of bot sensor
    #define capteurtop 3 : input pin of top sensor
    #define photoresistance A1 : input pin of light sensor ( can be used as analog or digital read)
    #define psu 7 : output pin for driving the led stair power supply unit (can control relay, or ATX psu(with reversing the logic))
    #define NB_MARCHES 17 : set numbers of useable stairs (min 3, max 24)
    #define lumfinale 85 : set the power of lights (min 1 max 255)

### Contributors #arduino-fr on freenode ###

* Alnoa
* Jer*****


### Who do I talk to? ###

* Repo owner or admin
* info@alnoa.fr or @alnoa on twitter