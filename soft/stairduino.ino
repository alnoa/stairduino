/*
|----------------------------------------------------------------------------------|
|                                    Stairduino                                    |
|                   Eclairage automatique séquentiel d'escalier                    |
|----------------------------------------------------------------------------------|
| -BEERWARE- Révision 42:                                                          |
| * Alnoa a créé ce fichier. Tant que vous conservez cet avertissement,            |
| * vous pouvez faire ce que vous voulez de ce truc. Si on se rencontre un jour et |
| * que vous pensez que ce truc vaut le coup, vous pouvez me payer une bière en    |
| * retour.                                                                        |
|                                                                                  |
| Alexandre                                                                        |
|----------------------------------------------------------------------------------|
| upverter >> https://upverter.com/eda/#tool=schematic,designId=4c14758326a713d1   |
|----------------------------------------------------------------------------------|
| GIT https://bitbucket.org/alnoa/stairduino/overview                              |
|----------------------------------------------------------------------------------|
*/

/**
 * mettre un 100nf a chaque bande et /ou reduire la vitesse PWM ou
 * augmenter la resistance devant irf540 à 2.7k
 *
 * link;
 * http://chaosworship.net/2013/01/08/pwm-led-driver-design-difficulties/
 * http://joost.damad.be/2012/09/dimming-12v-led-strip-with-mosfet-and.html
 */

  #define Version 1.00


//Libraries
  #include <LiquidCrystal_I2C.h>
  #include <Wire.h> 
  //+ ShifPWM plus loin /!\

//variables uniquement pour le compileur
  #define capteurbot 5
  #define capteurtop 3
  #define nbregistres  3//nb de shiftregisters en séries
  #define photoresistance A1
  #define psu 7//pin contrôlant l'alimentation des leds
  #define pwmFrequency  75//max 160 reco 75
  #define NB_MARCHES 17
  #define lumfinale 85 // lumisotsité de l escalier (rendu)

//tableau de la resolution d'animation
//
  float gaussValeurs[]={0.00,0.01,0.02,0.03,0.05,0.10,0.15,0.30,0.45,0.60,0.75,0.85,0.90,0.95,1.00};
  #define GAUSS_STEPS (sizeof(gaussValeurs)/sizeof(gaussValeurs[0]))

//Factorisation de deux variables pilotantes
//
  typedef struct {
    unsigned int valeur;
    bool ledON;  
  } Marche_data;

//création du tableau stockant ces facto.variables 
//
  Marche_data marche[NB_MARCHES];

//seuil de déclenchement de la marche suivante
  float seuil = gaussValeurs[int((sizeof(gaussValeurs)/sizeof(gaussValeurs[0]))/2)];


//paramètres Lcd I2c
  LiquidCrystal_I2C lcd(0x27,16,2);  //adresse  0x27  16 colonnes  et  2 lignes
  //Pins I2c: sda A4, scl A5 (uno)

//paramètres ShiftPWM et animations
  const int ShiftPWM_latchPin=10;
 // #define SHIFTPWM_NOSPI
 // const int ShiftPWM_dataPin = 11;
 // const int ShiftPWM_clockPin = 13;
  const bool ShiftPWM_invertOutputs = false;// npn ou pnp ?
  const bool ShiftPWM_balanceLoad = false;
  unsigned char lummMax = 255;//ce nombre est la résolution du fading
  unsigned int fadingMode = 0; // index de lummMax
  unsigned int tenirpos = 100; // pause en ms entre chaque marche
  unsigned int vitfade = 60; // delay en Ms pour la vitesse du fading
  unsigned int tempo = 5000;//temps de pause entre l’allumage et l’extinction

  #include <ShiftPWM.h>   // inclure ShiftPWM.h après les paramètres
  // Pins SPI:
  //- Data pin is MOSI (Uno and earlier: 11, Leonardo: ICSP 4, Mega: 51, Teensy 2.0: 2, Teensy 2.0++: 22) 
  //- Clock pin is SCK (Uno and earlier: 13, Leonardo: ICSP 3, Mega: 52, Teensy 2.0: 1, Teensy 2.0++: 21)

//variables  générales
  boolean etatveille=false;
  boolean prevetatveille;


void setup()
{
//decla des sorties 
  pinMode(capteurbot, INPUT_PULLUP);
  pinMode(capteurtop, INPUT_PULLUP);
  pinMode(psu, OUTPUT);
  pinMode(photoresistance, INPUT);
//initialisation de du lcd
  lcd.init();
  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight
  lcd.backlight(); //active le rétro-éclairage,  lcd.noBacklight() le désactive
//initialisation des paramètres pour la librairie ShiftPWM
  ShiftPWM.SetAmountOfRegisters(nbregistres);
  ShiftPWM.SetPinGrouping(1);
  ShiftPWM.Start(pwmFrequency,lummMax);
  ShiftPWM.SetAll(0);
//écran d'accueil
    lcd.print("Stairduino v");
    lcd.print(Version);
    lcd.setCursor(0,1);
    lcd.print("par alnoa.fr    ");
//test de toutes les sorties de la carte 
  digitalWrite(psu,HIGH);
  delay(500);
  for(int p=0; p<5; p++){
    ShiftPWM.SetAll(250);
    delay(500);
    ShiftPWM.SetAll(0); 
    delay(500);
  }
  digitalWrite(psu,LOW);
  //init des varibles marche[x].*
  raz_marches_monte();
  
  lcd.clear();
}

void loop()
{

  lcd.setCursor(0,0);
  lcd.print("En attente...");
  lcd.setCursor(0,1);

  /*
  
   if(detecnuit() == true)
  {
    digitalWrite(psu, HIGH);
    etatveille=true;
  }else{
    digitalWrite(psu, LOW);
    etatveille=false;
  }

  veille();

  */ 
  
  if(detecnuit() == true)
  {
    lcd.print("Nuit");
  }else{
    lcd.print("Jour");
  } 

  if(digitalRead(capteurbot)==HIGH && detecnuit() == true )
  {
    digitalWrite(psu, HIGH);
    delay(350);
    monter();
    digitalWrite(psu, LOW);
  }
  
  if(digitalRead(capteurtop)==HIGH && detecnuit() == true )
  {
    digitalWrite(psu, HIGH);
    delay(350);
    descendre();
    digitalWrite(psu, LOW);
  }


}

void veille(){

  if(etatveille != false)
  {
    if(prevetatveille == false)
    {
      for(int i=0;i<=5;i++)
      {
        ShiftPWM.SetOne(0,i);
        ShiftPWM.SetOne(NB_MARCHES-1,i);
        delay(vitfade*2);
      }
    prevetatveille = true;
    }
  }else{
    ShiftPWM.SetOne(0,0);
    ShiftPWM.SetOne(NB_MARCHES-1,0);
    prevetatveille = false;
  }
}

void monter()
{

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("ON sens monte");

  marche[0].ledON = true;

  monteOn();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Tempo...");
  delay(tempo);
  raz_marches_monte();
  
  marche[0].ledON = true;

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("OFF sens monte");
  monteOff();
  raz_marches_monte();

  prevetatveille = false;   
  lcd.clear();
}

void monteOn()
{
  while(marche[NB_MARCHES-1].valeur <GAUSS_STEPS-1)
  {
    for(int jj=0; jj<NB_MARCHES; jj++)
    {
      if(marche[jj].ledON && marche[jj].valeur <GAUSS_STEPS-1)
      {//verifie que l on allume et que la valeur n est pas au max
        marche[jj].valeur++;//passe à la valeur suivante
      }
      if(jj>0)
      {
        if (marche[jj-1].ledON && gaussValeurs[marche[jj-1].valeur] >= seuil)
        {//verifie ou se situe la marche precedente /seuil
        marche[jj].ledON = true;//passe la marche en etat montant , permet son allumage au debut de la boucle
        }
      }
      ShiftPWM.SetOne(jj,gaussValeurs[marche[jj].valeur]*lumfinale);
    }
    delay(vitfade);
  }
}

void monteOff()
{
 while(marche[NB_MARCHES-1].valeur <GAUSS_STEPS-1)
 {
    for(int jj=0; jj<NB_MARCHES; jj++)
    {
      if(marche[jj].ledON && marche[jj].valeur <GAUSS_STEPS-1)
      {//verifie que l on allume et que la valeur n est pas au max
        marche[jj].valeur++;//passe à la valeur suivante
      }
      if(jj>0)
      {
        if (marche[jj-1].ledON && gaussValeurs[marche[jj-1].valeur] >= seuil)
        {//verifie ou se situe la marche precedente /seuil
        marche[jj].ledON = true;//passe la marche en etat montant , permet son allumage au debut de la boucle
        }
      }
      ShiftPWM.SetOne(jj,((1-gaussValeurs[marche[jj].valeur])*lumfinale));
    }
    delay(vitfade);
  }
}

void descendre()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("ON sens desc");
  marche[NB_MARCHES-1].ledON=true;
  descOn();
  delay(tempo);
  raz_marches_monte();
  marche[NB_MARCHES-1].ledON=true;
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("OFF sens desc");
  descOff();
  raz_marches_monte();
  lcd.clear();
  prevetatveille = false;
}

void descOn()
{
  while(marche[0].valeur <GAUSS_STEPS-1)
  {
    for(int jj=NB_MARCHES-1; jj>=0; jj--)
    {
      if(marche[jj].ledON && marche[jj].valeur <GAUSS_STEPS-1)
      {//verifie que l on allume et que la valeur n est pas au max
        marche[jj].valeur++;//passe à la valeur suivante
      }
      if(jj<NB_MARCHES-1)
      {
        if (marche[jj+1].ledON && gaussValeurs[marche[jj+1].valeur] >= seuil)
        {//verifie ou se situe la marche precedente /seuil
        marche[jj].ledON = true;//passe la marche en etat montant , permet son allumage au debut de la boucle
        }
      }
      ShiftPWM.SetOne(jj,gaussValeurs[marche[jj].valeur]*lumfinale);
    }
    delay(vitfade);
  }
}

void descOff(){
  while(marche[0].valeur <GAUSS_STEPS-1)
  {
    for(int jj=NB_MARCHES-1; jj>=0; jj--)
    {
      if(marche[jj].ledON && marche[jj].valeur <GAUSS_STEPS-1)
      {//verifie que l on allume et que la valeur n est pas au max
        marche[jj].valeur++;//passe à la valeur suivante
      }
      if(jj<NB_MARCHES-1)
      {
        if (marche[jj+1].ledON && gaussValeurs[marche[jj+1].valeur] >= seuil)
        {//verifie ou se situe la marche precedente /seuil
        marche[jj].ledON = true;//passe la marche en etat montant , permet son allumage au debut de la boucle
        }
      }
      ShiftPWM.SetOne(jj,(1-gaussValeurs[marche[jj].valeur])*lumfinale);
    }
    delay(vitfade);
  }
}

boolean detecnuit()
{
  if((digitalRead(photoresistance)) == HIGH)
  {
    return true;
  }else{
    return false;
  }
}

void raz_marches_monte()
{
  for (int ii = 0; ii < NB_MARCHES; ii++)
  {
    marche[ii].valeur = 0;
    marche[ii].ledON = false;
  }
}

